import http.server
import socketserver
from whatsmyip.ip import get_ip
from urllib.parse import urlparse, parse_qs, urlencode
from whatsmyip.providers import HttpbinProvider
from string import Template


def apply_Template(file):
    d = {
            'IP_ADDRESS': get_ip(HttpbinProvider),
            'true':'$true'
        }
    with open(file, 'r') as f:
        src = Template(f.read())
        result = src.substitute(d)
        return bytes(result, "utf8")

class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(apply_Template('index.html'))
            return
        if self.path == '/myIP':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            ip = get_ip(HttpbinProvider)
            html = f"<html><head></head><body><h1>Hello {ip}!</h1></body></html>"
            self.wfile.write(bytes(html, "utf8"))
            return
        if self.path == '/exec_download.js':
            self.send_response(200)
            self.send_header("Content-type", "text/javascript")
            self.end_headers()
            self.wfile.write(apply_Template('exec_download.js'))
            return
        if self.path == '/proof.txt':
            self.send_response(200)
            self.send_header("Content-type", "text/javascript")
            self.end_headers()
            self.wfile.write(apply_Template('proof.txt'))
            return
        return http.server.SimpleHTTPRequestHandler.do_GET(self)

# Create an object of the above class
handler_object = MyHttpRequestHandler

PORT = 8000
my_server = socketserver.TCPServer(("", PORT), handler_object)

# Star the server
my_server.serve_forever()