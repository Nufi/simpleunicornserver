FROM python:alpine as builder

ENV PATH=$PATH:/usr/share/metasploit-framework
RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/community" >> /etc/apk/repositories && \
        apk add -U --no-cache \
        build-base \
        ruby \
        ruby-bigdecimal \
        ruby-bundler \
        ruby-io-console \
        ruby-webrick \
        ruby-dev \
        libffi-dev\
        openssl-dev \
        readline-dev \
        sqlite-dev \
        postgresql-dev \
        libpcap-dev \
        libxml2-dev \
        libxslt-dev \
        yaml-dev \
        zlib-dev \
        ncurses-dev \
        autoconf \
        bison \
        subversion \
        git \
        sqlite \
        nmap \
        libxslt \
        postgresql \
        ncurses \
        screen

RUN cd /usr/share && \
    git clone https://github.com/rapid7/metasploit-framework.git 
RUN cd /usr/share/metasploit-framework && \
    /usr/bin/bundle update --bundler && \
    /usr/bin/bundle install

RUN apk del \
    build-base \
    ruby-dev \
    libffi-dev\
    libressl-dev \
    readline-dev \
    sqlite-dev \
    postgresql-dev \
    libpcap-dev \
    libxml2-dev \
    libxslt-dev \
    yaml-dev \
    zlib-dev \
    ncurses-dev \
    bison \
    autoconf && \
    rm -rf /var/cache/apk/*

FROM builder

WORKDIR /usr/src/myapp
RUN git clone https://github.com/trustedsec/unicorn.git  /usr/src/unicorn

COPY src/requirements.txt /usr/src/myapp/
RUN pip3 install -r requirements.txt

COPY src/ /usr/src/myapp/
RUN pip3 install -r requirements.txt && \
    chmod +x /usr/src/myapp/start.sh
CMD /usr/src/myapp/start.sh