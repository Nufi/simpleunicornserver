# Simple Unicorn Server

- detects the public ip
- generates reverse meterpreter powershell payload
- opens a listener
- waits for connections

```
docker run --rm -p 80:8000 -p 443:443 -it registry.gitlab.com/nufi/simpleunicornserver
``